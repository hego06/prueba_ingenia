<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'Jose Perez',
            'email' => 'example@example.com',
            'password' => bcrypt('secret')
        ];

        $user = new User($data);
        $user->save();
        
    }
}
