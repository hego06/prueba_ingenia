<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="/css/app.css">
        <!-- Styles -->
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{route('customer.index')}}">Prueba ingenia</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('customer.index')}}">Index</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('customer.create')}}">Agregar Cliente</a>
                    </li>
                    <li>
                        <form action="{{route('logout')}}" method="post" class="inline">
                            @csrf
                            <button class="btn btn-danger">Salir</button>
                        </form>
                    </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="container-fluent">
            <div class="container my-3 p-3 bg-white rounded box-shadow">
            @yield('content')
            </div>
        </div>
        <script src="/js/app.js"></script>
        <script src="/js/formValidateCustomer.js"></script>
        @stack('scripts')
    </body>
</html>
