@if (count($messages))
  @foreach ($messages as $message)
  <script>
    Swal.fire({
      position: 'top-end',
      type: "{{$message['level']}}",
      title: "{{$message['message']}}",
      showConfirmButton: false,
      timer: 1500
    })
  </script>
  @endforeach
@endif