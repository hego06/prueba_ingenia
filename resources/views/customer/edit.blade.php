@extends('welcome')
@section('content')
<div class="card">
<div class="card-header text-center">
    <h3>Editar datos de cliente</h3>
</div>
<div class="card-body">
<form action="{{route('customer.update', $customer)}}" method="POST" onSubmit="return validateFormCustomer();">
    @csrf
    {{method_field('PUT')}}
    @include('customer.form')
</form>
</div>
</div>
@push('scripts')
@include('partials.messages')
@endpush
@endsection