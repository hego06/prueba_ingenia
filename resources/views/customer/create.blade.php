@extends('welcome')
@section('content')
<div class="card ">
<div class="card-header text-center">
    <h3>Registrar nuevo cliente</h3>
</div>
<div class="card-body">
<form action="{{route('customer.store')}}" method="POST" onSubmit="return validateFormCustomer();">
    @csrf
    @include('customer.form')
</form>
</div>
</div>
@endsection
@push('name')
@endpush