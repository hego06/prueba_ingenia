<div class="form-group">
        <label for="name">Nombre(s)</label>
        <input 
        type="text"
        id="firstName"
        name="name"
        class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" 
        value="{{old('name', isset($customer->name) ? $customer->name : '')}}"
        >
        {!!$errors->first('name', '<span class="help-block text-danger">:message</span>')!!}
    </div>
    <div class="form-group">
        <label for="last_name">Apellidos(s)</label>
        <input 
        type="text"
        id="lastName" 
        name="last_name"
        class="form-control {{$errors->has('last_name') ? 'is-invalid' : ''}}"
        value="{{old('last_name', isset($customer->last_name) ? $customer->last_name : '')}}"
        >
        {!! $errors->first('last_name', '<span class="help-blok text-danger">:message</span>')!!}
    </div>
    <div class="form-group">
        <label for="email">Correo</label>
        <input 
        type="text"
        id="email"
        name="email"
        class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}"
        value="{{old('email', isset($customer->email) ? $customer->email : '')}}"
        >
        {!! $errors->first('email', '<span class="help-blok text-danger">:message</span>')!!}
    </div>
    <div class="form-group">
        <label for="credit_card">Número de tarjeta</label>
        <input 
        type="text" 
        id="creditCard"
        name="credit_card"
        class="form-control {{$errors->has('credit_card') ? 'is-invalid' : ''}}"
        value="{{old('credit_card', isset($customer->credit_card) ? $customer->credit_card : '')}}"
        >
        {!! $errors->first('credit_card', '<span class="help-blok text-danger">:message</span>')!!}
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-success">Eviar</button>
    </div>