@extends('welcome')
@section('content')
<div class="row">
    <div class="col-md-12">
    <table class="table table-sm table-bordered table-striped">
        <thead>
            <th>#</th>
            <th>Nombre(s)</th>
            <th>Apellido(s)</th>
            <th>Correo</th>
            <th>Número de tajeta</th>
            <th>Acciones</th>
        </thead>
        <tbody>
            @foreach ($customers as $customer)
            <tr>
                <td>{{$customer->id}}</td>
                <td>{{$customer->name}}</td>
                <td>{{$customer->last_name}}</td>
                <td>{{$customer->email}}</td>
                <td>{{$customer->credit_card}}</td>
                <td>
                    <a href="{{route('customer.edit', $customer)}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
                    <form action="{{route('customer.destroy', $customer)}}" method="POST" style="display: inline;">
                        @csrf
                        {{method_field('DELETE')}}
                        <input type="hidden" name="redirects_to" value="{{ $customers->url($customers->currentPage())}}">
                        <button href="#" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$customers->links()}}
    </div>
</div>
@endsection
@push('scripts')
@include('partials.messages')
@endpush