<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'auth'],function(){
    Route::get('/','CustomerController@index')->name('customer.index');
    Route::get('customers/{customer}/edit','CustomerController@edit')->name('customer.edit');
    Route::put('customers/{customer}','CustomerController@update')->name('customer.update');
    Route::get('customers/create','CustomerController@create')->name('customer.create');
    Route::post('customers','CustomerController@store')->name('customer.store');
    Route::delete('customers/{customer}','CustomerController@destroy')->name('customer.destroy');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
