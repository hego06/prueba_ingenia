function validateFormCustomer()
{
    countError = 0;
    first_name = document.getElementById('firstName');
    lastName = document.getElementById('lastName');
    email = document.getElementById('email');
    credit_card = document.getElementById('creditCard');

    if(first_name.value === ""){
        first_name.classList.add('is-invalid');
        countError++;
    }else{
        first_name.classList.remove('is-invalid');
    }
    if(lastName.value === ""){
        lastName.classList.add('is-invalid');
        countError++;
    }else{
        lastName.classList.remove('is-invalid');
    }

    if(email.value === "" || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value) != true){
        email.classList.add('is-invalid');
        countError++;
    }else{
        email.classList.remove('is-invalid');
    }

    if(credit_card.value === "" || isNaN(credit_card.value)){
        credit_card.classList.add('is-invalid');
        countError++;
    }else{
        credit_card.classList.remove('is-invalid');
    }
    if (countError > 0)
    {
      return false
    }else{
        return true
    }
}