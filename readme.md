### Requirements

PHP >= 7.1.3
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Ctype PHP Extension
JSON PHP Extension
BCMath PHP Extension

### Installation

1.- After clone or download the repository, install the dependencies
```
composer install
```
2.- Configurate database connection in .env file, create a APP_KEY
```
php artisan key:generate
composer dump-autoload
```
3.- Update composer autoload
```
composer dump-autoload
```
4.- execute migration and seeder 

```
php artisan migrate
php artisan db:seed
```

5.- Execute the laravel app
```
php artisan ser
```

### User Credentials

```
email: example@example.com
pass : secret
```

