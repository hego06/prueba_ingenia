<?php

namespace App;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name', 'last_name', 'email', 'credit_card'];

    public function getCreditCardAttribute($creditCard)
    {
        return Crypt::decrypt($creditCard);
    }

    public function setCreditCardAttribute($creditCard)
    {
        $this->attributes['credit_card'] =  Crypt::encrypt($creditCard);
    }
}
